const printBoard = (board, rootElement) => {
    board.forEach(cells => {
        const next = document.createElement('div');
        cells.forEach(nextCell => {
            const cell = document.createElement('div');
            cell.style.cssFloat = 'left';
            cell.style.border = 'solid darkgrey 1px';
            cell.style.height = '10px';
            cell.style.width = '10px';
            cell.style.backgroundColor = nextCell === 1 ? 'green' : '';

            rootElement.appendChild(cell);
        });
        next.style.clear = 'left';
        rootElement.appendChild(next);
    });
};

const updateBoard = async (board, rootElement) => {
    let x = 0;
    let y = 0;
    const items = [...rootElement.children];
    for (let cell of items) {
        if (cell.style.clear === 'left') {
            x++;
            y = 0;
        } else {
            y++;
        }
        if (x < 50) {
            cell.style.backgroundColor = board[x][y] === 1 ? 'green' : '';
        }
    }
};

const rootElement = document.getElementById('root');
let board = generateBoard();
let generation = 0;
window.requestAnimationFrame(() => printBoard(board, rootElement));

const nextFrame = (board) => {
    generation++;
    board = generateNextGeneration(board);
    window.requestAnimationFrame(() => updateBoard(board, rootElement));
    window.requestAnimationFrame(() => nextFrame(board));
};
window.requestAnimationFrame(() => nextFrame(board));
