/**
 * Compute the first generation at random
 * @returns [[number]]
 */
const generateBoard = () => {
    const board = [];
    for (let i = 0; i < 50; i++) {
        board[i] = [];
        for (let j = 0; j < 50; j++) {
            board[i][j] = Math.round(Math.random());
        }
    }
    return board;
};

/**
 * Compute the next generation for each elements in the board.
 * @param board
 * @returns [[number]]
 */
const generateNextGeneration = (board) => {
    return board.map((val, indexX) => {
        return val.map((cell, indexY) => {
            return getNextCellState(board, indexX, indexY)
        });
    });
};

/**
 * Compute the current cell state for the next generation
 * @param board
 * @param indexX
 * @param indexY
 * @returns {number}
 */
const getNextCellState = (board, indexX, indexY) => {
    const livingCells = getAdjacentAlive(board, indexX, indexY);
    let nextState;

    if (livingCells < 2 || livingCells > 3) {
        nextState = 0;
    } else if (livingCells === 2) {
        nextState = board[indexX][indexY];
    } else {
        nextState = 1;
    }
    return nextState;
};

/**
 * Return closure that will get a cell value from a two by two array
 * @param x
 * @param y
 * @returns {function(*, *=, *=): *}
 */
const getCellValue = (x, y) => {
    return (board, indexX, indexY) => {
        const nextX = getClosestIndex(indexX, x, 50);
        const nextY = getClosestIndex(indexY, y, 50);
        return board[nextX][nextY];
    };
};

/**
 *
 * @param index
 * @param flow
 * @param max
 * @returns {*}
 */
const getClosestIndex = (index, flow, max) => {
    const next = index + flow;
    return next >= 0 ?
        next >= max ?
            0 + flow :
            next
        : max + flow;
};
/** Get top line cells from current **/
const getUpperLeft = getCellValue(-1, -1);
const getUpperCenter = getCellValue(-1, 0);
const getUpperRight = getCellValue(-1, 1);

/** Get same line cells from current **/
const getLeft = getCellValue(0, -1);
const getRight = getCellValue(0, 1);

/** Get bottom line cells from current **/
const getBottomLeft = getCellValue(1, -1);
const getBottomCenter = getCellValue(1, 0);
const getBottomRight = getCellValue(1, 1);

/**
 * Count all nearby alive cells
 * @param board
 * @param indexX
 * @param indexY
 * @returns number
 */
const getAdjacentAlive = (board, indexX, indexY) => {
    return getUpperLeft(board, indexX, indexY) + getUpperCenter(board, indexX, indexY) + getUpperRight(board, indexX, indexY) +
        getLeft(board, indexX, indexY) + getRight(board, indexX, indexY) +
        getBottomLeft(board, indexX, indexY) + getBottomCenter(board, indexX, indexY) + getBottomRight(board, indexX, indexY);
};
